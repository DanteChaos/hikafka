# HiKafka

A demo project in using kafka to produce and consume messages

## Requirements

- Docker
- Composer

## Usage

Clone from gitlab
```
git clone git@gitlab.com:DanteChaos/hikafka.git
```

Run composer
```
composer install --ignore-platform-reqs
```

Boot up docker

```
docker-compose up -d
```

Run the script Requester.php
```
docker-compose exec php php ./src/Requester.php
```

This will give 2 possible outputs
- Throws an error: No Response
- Shows a message with a random name