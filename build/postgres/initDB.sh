#!/bin/sh
set -e
psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
    create schema broker;

    create table if not exists broker.request
    (
        id serial not null
            constraint request_pk
                primary key,
        token varchar not null,
        data varchar default null,
        datetime timestamp not null
    );

    create unique index request_token_uindex
        on broker.request (token);
EOSQL