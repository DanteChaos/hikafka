<?php
declare(strict_types=1);

use App\Exceptions\NoResponseException;
use App\Services\Database;
use App\Services\Broker;
use App\Services\Kafka;
use RdKafka\Consumer;
use RdKafka\Producer;

require '../vendor/autoload.php';

$method = $_GET['method'];
$message = $_GET['message'];
$token = $_GET['token'] ?? null;

$connectionParams = [
    'dbname' => getenv('DATABASE_NAME'),
    'user' => getenv('DATABASE_USER'),
    'password' => getenv('DATABASE_PASSWORD'),
    'host' => getenv('DATABASE_HOST'),
    'port' => '5432',
    'driver' => 'pgsql',
];

$database = new Database($connectionParams);

$conf = new RdKafka\Conf();
$consumer = new Consumer($conf);
$consumer->addBrokers('kafka-server1');

$conf = new RdKafka\Conf();
$producer = new Producer($conf);
$producer->addBrokers('kafka-server1');
$kafka = new Kafka($consumer, $producer, $database);

$broker = new Broker($database, $kafka);
$response = null;
if ($method === 'request_status') {
    try {
        $response = $broker->retrieveRecord($token);
    } catch (NoResponseException $exception) {
        $response = $exception->getMessage();
    }
}

if ($method === 'request_token') {
    try {
        $response = $broker->publish($message);
    } catch (Exception $exception){
        $response = $exception->getMessage();
    }
}

if ($method === 'update_message') {
    $broker->updateMessage($token, $message);
    $response = 'OK';
}

print json_encode($response, JSON_THROW_ON_ERROR);