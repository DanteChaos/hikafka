<?php

declare(strict_types=1);

use App\Exceptions\NoResponseException;
use App\Services\Requester;

require './vendor/autoload.php';

$requester = new Requester();
try {
    $token = $requester->requestToken();
} catch (Exception $exception) {
    echo 'Unable to retrieve Token' . PHP_EOL;
    error_log($exception->getMessage());
    exit(1);
}

try {
    echo $requester->pollBroker($token);
} catch (NoResponseException $exception) {
    echo $exception->getMessage() . PHP_EOL;
} catch (JsonException $exception) {
    error_log($exception->getMessage());
    echo 'Unable to read json response' . PHP_EOL;
}