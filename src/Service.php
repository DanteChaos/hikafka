<?php

declare(strict_types=1);

use App\Services\Database;
use App\Services\Kafka;
use RdKafka\Consumer;
use RdKafka\Producer;

require './vendor/autoload.php';

$topic = $argv[1];
if($topic === null){
    throw new \RuntimeException('Topic not set!');
}

$connectionParams = [
    'dbname' => getenv('DATABASE_NAME'),
    'user' => getenv('DATABASE_USER'),
    'password' => getenv('DATABASE_PASSWORD'),
    'host' => getenv('DATABASE_HOST'),
    'port' => '5432',
    'driver' => 'pgsql',
];

$database = new Database($connectionParams);

$conf = new RdKafka\Conf();
$consumer = new Consumer($conf);
$consumer->addBrokers('kafka-server1');

$conf = new RdKafka\Conf();
$producer = new Producer($conf);
$producer->addBrokers('kafka-server1');

$kafka = new Kafka($consumer, $producer, $database);
$kafka->listenOnTopic($topic);