<?php

declare(strict_types=1);

namespace App\Services;

class Broker
{
    private Database $database;

    private Kafka $kafka;

    public function __construct(Database $database, Kafka $kafka)
    {
        $this->database = $database;
        $this->kafka = $kafka;
    }

    public function publish(string $message): string
    {
        $token = $this->generateToken();

        $this->database->insertRecord($token);
        $this->kafka->setTopic('TopicA');
        $this->kafka->generateMessage(sprintf('%s-%s', $token, $message), 'TopicA');

        return $token;
    }

    private function generateToken(): string
    {
        $token = openssl_random_pseudo_bytes(16);

        return bin2hex($token);
    }

    public function retrieveRecord(string $token): ?string
    {
        $record = $this->database->retrieveRecord($token);
        if ($record !== false) {
            return $record['data'];
        }

        return '';
    }

    public function updateMessage(string $token, string $message): void
    {
        $this->kafka->setTopic('TopicB');
        $this->kafka->generateMessage(sprintf('%s-%s', $token, $message), 'TopicB');
    }
}