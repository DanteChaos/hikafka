<?php

declare(strict_types=1);

namespace App\Services;


class Curl
{
    /**
     * @return mixed
     * @throws \JsonException
     */
    public static function request(string $url, array $parameters): ?string
    {
        $request = curl_init();
        curl_setopt($request, CURLOPT_URL, $url . '?' . http_build_query($parameters));
        curl_setopt($request, CURLOPT_HEADER, 0);
        curl_setopt($request, CURLOPT_CONNECTTIMEOUT, 1);

        curl_setopt(
            $request, CURLOPT_RETURNTRANSFER,
            true
        );

        $result = curl_exec($request);
        if($result === false){
            $error = curl_error($request);
            throw new \Exception($error);
        }
        curl_close($request);

        return json_decode($result, true, 512, JSON_THROW_ON_ERROR);
    }
}