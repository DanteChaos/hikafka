<?php
declare(strict_types=1);

namespace App\Services;

use Carbon\Carbon;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Exception;
use PDO;

class Database
{
    private \PDO $connection;

    public function __construct($connectionParameters)
    {
        try {
            $this->connection = new \PDO(
                sprintf(
                    'pgsql:host=%s;port=%d;dbname=%s;user=%s;password=%s',
                    $connectionParameters['host'],
                    $connectionParameters['port'],
                    $connectionParameters['dbname'],
                    $connectionParameters['user'],
                    $connectionParameters['password']
                )
            );
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage());
        }
    }

    public function updateRecord(string $token, string $amendedValue): void
    {
        $this->connection->exec("UPDATE broker.request SET data = '{$amendedValue}' WHERE token = '{$token}';");
    }

    public function retrieveRecord(string $token)
    {
        $pdoStatement = $this->connection->query("SELECT data FROM broker.request WHERE token = '{$token}';");
        return $pdoStatement->fetch(PDO::FETCH_ASSOC);
    }

    public function insertRecord(string $token): void
    {
        $date = new \DateTime();
        $formatted = $date->format('Y-m-d H:i:s');
        $exec = $this->connection->exec("INSERT INTO broker.request (token, datetime) values ('{$token}', '{$formatted}');");
        if ($exec === false) {
            throw new \PDOException('Unable to insert record');
        }
    }
}