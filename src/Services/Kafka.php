<?php

declare(strict_types=1);

namespace App\Services;


use Doctrine\DBAL\Exception;
use RdKafka\Consumer;
use RdKafka\Producer;

class Kafka
{
    private const NAMES = [
        'Joao',
        'Bram',
        'Gabriel',
        'Fehim',
        'Eni',
        'Patrick',
        'Micha',
        'Mirzet',
        'Liliana',
        'Sebastien',
    ];

    private Consumer $consumer;

    private string $topic;

    private Producer $producer;

    /**
     * @var Database
     */
    private Database $database;

    public function __construct(Consumer $consumer, Producer $producer, Database $database)
    {
        $this->consumer = $consumer;
        $this->producer = $producer;
        $this->database = $database;
    }

    public function generateMessage(string $message, string $topic): void
    {
        $currentTopic = $this->producer->newTopic($topic);
        $currentTopic->produce(RD_KAFKA_PARTITION_UA, 0, $message);
        $this->producer->flush(10000);
    }

    public function listenOnTopic(string $topic): void
    {
        $this->setTopic($topic);
        $this->listen();
    }

    public function listen(): void
    {
        $topic = $this->consumer->newTopic($this->getTopic());
        echo 'Listening on ' . $this->getTopic() . PHP_EOL;
        $topic->consumeStart(0, RD_KAFKA_OFFSET_BEGINNING);
        while (true) {
            try {
                $message = $topic->consume(0, 1000);
                if ($message === null || $message->err === RD_KAFKA_RESP_ERR__PARTITION_EOF) {
                    continue;
                }

                if ($message->err) {
                    throw new \Exception($message->errstr());
                }
                $payload = $message->payload;
                if ($payload !== null) {
                    echo 'Updating message' . PHP_EOL;
                    $this->updateMessage($payload);
                }
            } catch (\Exception $exception) {
                echo $exception->getMessage() . PHP_EOL;
                error_log(sprintf("%s:%s %s", $exception->getFile(), $exception->getLine(), $exception->getMessage()));
            }
        }
    }

    public function getTopic(): string
    {
        return $this->topic;
    }

    public function setTopic(string $topic): void
    {
        $this->topic = $topic;
    }

    /**
     * @throws Exception|\JsonException
     */
    private function updateMessage(string $payload): void
    {
        if ($this->getTopic() === 'TopicA') {
            $splitter = explode('-', $payload);
            $message = sprintf('%s %s', $splitter[1], self::NAMES[array_rand(self::NAMES)]);
            Curl::request(
                'http://broker-web', ['method' => 'update_message', 'message' => $message, 'token' => $splitter[0]]
            );
        }
        if ($this->getTopic() === 'TopicB') {
            $splitter = explode('-', $payload);
            $message = sprintf('%s %s', $splitter[1], 'Bye');
            $this->database->updateRecord($splitter[0], $message);
        }
    }
}