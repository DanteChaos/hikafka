<?php

declare(strict_types=1);


namespace App\Services;


use App\Exceptions\NoResponseException;

class Requester
{
    /**
     * @return mixed|string|null
     * @throws \JsonException
     */
    public function requestToken()
    {
        return Curl::request('http://broker-web', ['method' => 'request_token', 'message' => 'Hi,']);
    }

    /**
     * @return string|null
     * @throws NoResponseException
     * @throws \JsonException
     */
    public function pollBroker(string $token): ?string
    {
        $timer = microtime(true);
        while (true) {
            $request = Curl::request('http://broker-web', ['method' => 'request_status', 'token' => $token]);
            if (!empty($request)) {
                return $request;
            }
            $subTimer = microtime(true);
            $execution_time = ($subTimer - $timer);
            if ($execution_time >= 1) {
                throw new NoResponseException('No Response', 408);
            }
            usleep(50000);
        }
    }
}