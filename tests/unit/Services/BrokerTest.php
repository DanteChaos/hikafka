<?php
declare(strict_types=1);

namespace Services;

use App\Services\Broker;
use App\Services\Database;
use App\Services\Kafka;
use Doctrine\DBAL\Exception;
use PHPUnit\Framework\TestCase;

class BrokerTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testPublish(): void
    {
        $database = $this->createMock(Database::class);
        $database->expects(self::once())->method('insertRecord');
        $kafka = $this->createMock(Kafka::class);
        $kafka->expects(self::once())->method('setTopic');
        $kafka->expects(self::once())->method('generateMessage');

        $broker = new Broker($database, $kafka);
        $broker->publish('Test');
    }

    public function testRetrieveRecord(): void
    {
        $database = $this->createMock(Database::class);
        $database->method('retrieveRecord')->willReturn(false);
        $database->expects(self::once())->method('retrieveRecord');
        $kafka = $this->createMock(Kafka::class);

        $broker = new Broker($database, $kafka);
        $record = $broker->retrieveRecord('1337');
        self::assertEquals('', $record);
    }
}
